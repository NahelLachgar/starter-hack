export const environment = {
  production: true,
  apiUrl: 'https://api.starterhack.fr',
  awsS3Bucket: 'starterhack-storage',
  awsS3Region: 'eu-west-3'
};
 