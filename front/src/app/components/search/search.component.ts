import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { User } from 'src/app/models/user.model';
import { UserService } from 'src/app/services/user.service';
import { ChallengeService } from 'src/app/services/challenge.service';
import { Challenge } from 'src/app/models/challenge.model';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {

  users: User[]
  challenges: Challenge[]
  
  constructor(private route: ActivatedRoute,
              private userService: UserService,
              private challengeService: ChallengeService) { }

  ngOnInit() {
    const query = this.route.snapshot.queryParams.query
    this.getUsers(query)
    this.getChallenges(query)
  }

  async getUsers(search: string) {
    this.users = await this.userService.searchUser(search)
  }

  async getChallenges(search: string) {
    this.challenges = await this.challengeService.searchChallenge(search)
  }

}
