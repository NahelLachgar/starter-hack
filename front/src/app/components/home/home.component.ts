import { Component, OnInit } from '@angular/core';
import { NewsService } from 'src/app/services/news.service';
import { News } from 'src/app/models/news.model';
import { Subscription } from 'rxjs';
import { ChallengeService } from 'src/app/services/challenge.service';
import { Challenge } from 'src/app/models/challenge.model';
import { UserService } from 'src/app/services/user.service';
import { ProposalService } from 'src/app/services/proposal.service';
import { DocService } from 'src/app/services/doc.service';
import { Documentation } from 'src/app/models/documentation.model';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  news: News
  challenges: Challenge[] = []
  challengesCount: number
  newsSubscription: Subscription
  documentations: Documentation[]
  documentationsSubcription: Subscription

  constructor(private newsService: NewsService,
              private challengeService: ChallengeService,
              private userService: UserService,
              private proposalsService: ProposalService,
              private docService: DocService) { }

  ngOnInit() {
    this.getNews()
    this.getChallenges()
    this.userService.getUsers()
    this.proposalsService.getProposals()
    this.documentationsSubcription = this.docService.docs$.subscribe((documentations: Documentation[]) => {
      this.documentations = documentations
    })
    this.docService.getDocs()
  }

  get usersCount() {
    return this.userService.users.length
  }

  get proposalsCount() {
    return this.proposalsService.proposals.length
  }
  
  get docsCount() {
    return this.docService.docs.length
  }

  getNews() {
    this.newsSubscription = this.newsService.news$.subscribe(
      (news: News) => {
        this.news = news
    })

    this.newsService.getNews()
  }

  getChallenges() {
    let unresolvedChallenges = []
    
    this.challengeService.getChallenges()
      .subscribe((challenges: Challenge[]) => {
        this.challengesCount = challenges.length
        challenges.forEach(challenge => {
          if (!this.getResolved(challenge.id))
            unresolvedChallenges.push(challenge)
        })
        this.challenges = unresolvedChallenges
      })
  }

  getResolved(challengeId: number): boolean {
    this.challengeService.hasResolvedChallenge(challengeId)
      .then(hasResolved => {
        return hasResolved
      })

    return false
  }

  getDocType(doc: Documentation) {
    return this.docService.getDocType(doc) == 'file' ? 'Document' : 'Page web'
  }

}
