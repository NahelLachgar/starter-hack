import { Component, OnInit, TemplateRef } from '@angular/core';
import { Challenge, ChallengeCategory } from 'src/app/models/challenge.model';
import { Documentation } from 'src/app/models/documentation.model';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ChallengeService } from 'src/app/services/challenge.service';
import { environment } from 'src/environments/environment';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { Proposal } from 'src/app/models/proposal.model';
import { ProposalService } from 'src/app/services/proposal.service';
import { ActivatedRoute, Router } from '@angular/router';
import { SweetAlertService } from 'src/app/services/sweet-alert.service';
import { DocService } from 'src/app/services/doc.service';
import { Subscription } from 'rxjs';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-challenge-add',
  templateUrl: './challenge-add.component.html',
  styleUrls: ['./challenge-add.component.scss']
})
export class ChallengeAddComponent implements OnInit {

  s3Bucket = environment.awsS3Bucket
  s3Region = environment.awsS3Region
  editor = ClassicEditor;
  challengeType: string;
  challengeForm: FormGroup
  scriptForm: FormGroup
  scriptData: any
  scriptsList: any
  docForm: FormGroup
  isEditing: boolean = false
  id: number
  challenge: Challenge
  challengeCategories: ChallengeCategory[]
  showAddScript: boolean = false
  docsSubscription: Subscription;
  docs: Documentation[] = []
  selectedDocs: Documentation[] = []
  proposal: Proposal = null
  bsModalRef: BsModalRef;
  currentDoc: Documentation;

  constructor(private challengeService: ChallengeService,
    private proposalService: ProposalService,
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private swService: SweetAlertService,
    private docService: DocService,
    private modalService: BsModalService) { }

  get isStatic() {
    return !!this.challengeType && this.challengeType == 'static'
  }

  get isDynamic() {
    return !!this.challengeType && this.challengeType == 'dynamic'
  }

  getDocType(doc: Documentation) {
    return this.docService.getDocType(doc) == 'file' ? 'Document' : 'Page web'
  }

  ngOnInit() {
    this.initForms()
    this.getDocs()
    this.initScripts()
    this.id = this.route.snapshot.params.id;

    if (this.id) {
      this.isEditing = true
      this.getChallenge()
    }

    this.getCategories()
    this.scriptForm = this.formBuilder.group({
      script: [null]
    })

    this.scriptData = {
      url: null,
      sent: false
    }
  }

  getCategories() {
    this.challengeService.getCategories().subscribe(
      (categories: ChallengeCategory[]) => this.challengeCategories = categories
    )
  }

  getDocs() {
    this.docsSubscription = this.docService.docs$.subscribe(
      (docs: Documentation[]) => this.docs = docs
    )
    this.docService.getDocs()
  }

  initForms() {
    let challenge: Challenge;
    challenge = this.isEditing ? this.challenge : null
    this.challengeForm = this.formBuilder.group({
      name: [this.isEditing ? challenge.name : "", Validators.required],
      description: [this.isEditing ? challenge.description : "", Validators.required],
      points: [this.isEditing ? challenge.points : "", Validators.required],
      level: [this.isEditing ? challenge.level : "", Validators.required],
      flag: [this.isEditing ? challenge.flag : "", Validators.required],
      timer: [this.isEditing ? challenge.timer : "", Validators.required],
      amiId: [this.isEditing ? challenge.amiId : "", Validators.required],
      script: [this.isEditing ? challenge.script : "", Validators.required],
      host: [this.isEditing ? challenge.host : "", Validators.required],
      category: [this.isEditing ? challenge.category.name : "", Validators.required],
      docs: [this.isEditing ? challenge.docs : []]
    })

    if (this.route.snapshot.queryParamMap.get("proposal")) {
      this.proposalService.getProposal(
        parseInt(this.route.snapshot.queryParamMap.get("proposal")))
        .subscribe((proposal) => {
          if (!proposal) {
            this.router.navigateByUrl("/challenges")
          } else if (proposal.status != "NEW" && proposal.status != "PROCESSED") {
            this.proposal = proposal
          } else {
            this.router.navigateByUrl("/challenges")
          }
        })
    }
  }

  initScripts() {
    this.scriptsList = this.challengeService.getScripts()
  }

  submit() {
    if (!this.isChallengeValid) return false;
    this.challenge = this.challengeForm.value
    this.challenge.script = this.scriptData.url
    this.challenge.docs = this.selectedDocs
    this.challenge.category = this.challengeCategories.find(category => category.name == this.challengeForm.value.category)
    if (this.isEditing) {
      this.challenge.id = this.id
      if (this.showAddScript)
        this.challenge.script = this.scriptData.url
      else
        this.challenge.script = this.scriptForm.get('script').value

      this.editChallenge(this.challenge)
    }
    else {
      if (this.showAddScript) this.challenge.script = this.scriptData.url
      else this.challenge.script = this.scriptForm.get('script').value
      this.addChallenge(this.challenge)
    }
  }

  editChallenge(challenge) {
    this.challengeService.updateChallenge(challenge)
      .subscribe(
        (challenge: Challenge) => {
          this.router.navigateByUrl('/challenges')
          this.swService.sendAlert('Le challenge a été mis à jour ! ', 'success', () => {
            this.router.navigateByUrl('/challenges')
          })
        },
        err => {
          console.error(err)
        })
  }

  addChallenge(challenge) {
    this.challengeService.addChallenge(challenge)
      .subscribe(
        (challenge: Challenge) => {
          if (!this.proposal) {
            this.router.navigateByUrl('/challenges')
            this.swService.sendAlert('Le challenge a été ajouté ! ', 'success', () => {
              this.router.navigateByUrl('/challenges')
            })
          } else {
            this.acceptProposal()
          }
        },
        err => {
          console.error(err)
        })
  }

  acceptProposal() {
    this.proposalService.acceptProposal(this.proposal.id, 0, "PROCESSED")
      .subscribe(_ => {
        this.router.navigateByUrl("/challenges")
      },
        err => console.error(err))
  }

  uploadFile(event) {
    const file = (event.target as HTMLInputElement).files[0];
    this.scriptForm.patchValue({
      'script': file
    })

    this.scriptForm.get('script').updateValueAndValidity()
  }

  submitScript() {
    var formData = new FormData();
    formData.append("script", this.scriptForm.get('script').value);
    this.challengeService.uploadScript(formData).subscribe((link: string) => {
      this.scriptData = {
        url: link,
        sent: true
      }
      this.swService.sendAlert('Le script a été envoyé !', 'success')
      setTimeout(() => this.scriptData.sent = false, 5000)
    }, err => this.swService.sendAlert('Ce script existe déjà', 'error'))
  }

  getCategory(categoryName: string) {
    return this.challengeCategories.find(category => category.name == categoryName)
  }

  get challengeCategory() {
    if (this.challenge) return this.getCategory(this.challenge.category.name).name
    return null
  }

  getChallenge() {
    const id = this.route.snapshot.params.id;
    this.challengeService.getChallenge(id)
      .subscribe(challenge => {
        if (challenge) {
          this.challenge = challenge;
          this.selectedDocs = challenge.docs
          this.docs = this.docs.filter(doc => {
            return !this.challenge.docs.find(d => d.id == doc.id)
          })
          this.challengeType = challenge.host ? 'static' : 'dynamic'
          this.initForms()
        }
      },
        err => console.error(err))
  }

  onShowAddScript() {
    this.initScripts()
    this.showAddScript = !this.showAddScript
  }

  get isChallengeValid() {
    const controls = [
      this.challengeForm.get('name'),
      this.challengeForm.get('description'),
      this.challengeForm.get('points'),
      this.challengeForm.get('level'),
      this.challengeForm.get('flag'),
      this.challengeForm.get('points'),
      this.challengeForm.get('timer'),
      this.challengeForm.get('category'),
    ]

    if (this.isDynamic) {
      controls.concat([
        this.challengeForm.get('script'),
      ])
    } else {
      controls.concat([
        this.challengeForm.get('host'),
        this.challengeForm.get('script'),
      ])
    }
    controls.forEach(c => {
      if (c.invalid) return false
    })
    return true
  }

  openModal(template: TemplateRef<any>) {
    this.bsModalRef = this.modalService.show(template, { class: 'modal-lg' });
  }

  openPdf(template: TemplateRef<any>, doc: Documentation) {
    this.currentDoc = doc
    this.bsModalRef = this.modalService.show(template, { class: 'modal-lg' })
  }

  onDocAdd(doc: Documentation) {
    this.selectedDocs.push(doc)
    this.bsModalRef.hide();
  }

  addDoc() {
    const doc = this.docs.find(doc => doc.name == this.challengeForm.value.docs)
    if (this.selectedDocs.includes(doc) || !doc ) return false
    this.selectedDocs.push(doc)
    this.docs.splice(this.docs.indexOf(doc), 1)
  }

  removeDoc(index: number) {
    const doc = this.selectedDocs[index]
    this.selectedDocs.splice(index, 1)
    this.docs.push(doc)
  }

  get iframeHeight() {
    return (window.innerHeight * 0.9).toString() + 'px'
  }
}