import { Component, OnInit, OnDestroy } from '@angular/core';
import { Challenge } from '../../../models/challenge.model';
import { Observable, Subscription } from 'rxjs';
import { ChallengeService } from '../../../services/challenge.service'
import { AuthService } from 'src/app/services/auth.service';
import { ChallengeState } from 'src/app/models/challengeState.model';
import { ActivatedRoute } from '@angular/router';
import { SweetAlertService } from 'src/app/services/sweet-alert.service';

@Component({
  selector: 'app-challenge-list',
  templateUrl: './challenge-list.component.html',
  styleUrls: ['./challenge-list.component.scss']
})
export class ChallengeListComponent implements OnInit, OnDestroy {

  challenges: Challenge[] = [];
  resolvedChallenges: Challenge[] = [];
  categoryName: string;
  queryParamsSub: Subscription

  constructor(private challengeService: ChallengeService,
    private authService: AuthService,
    private route: ActivatedRoute,
    private swService: SweetAlertService) { }

  ngOnInit() {
    this.queryParamsSub = this.route.queryParams.subscribe(p => {
      if (p.category && p.category != this.categoryName || !p.category) {
        this.initChallenges()
      }
    });
    this.initChallenges()
  }

  ngOnDestroy() {
    this.queryParamsSub.unsubscribe()
  }

  initChallenges() {
    this.categoryName = this.route.snapshot.queryParams.category
    this.challengeService.getResolvedChallenges().then(
      (challenges: Challenge[]) => {
        this.resolvedChallenges = challenges
        this.getChallenges()
      }
    )
  }

  getChallenges() {
    let observable: Observable<Challenge[]>;
    if (this.categoryName) observable = this.challengeService.getChallengesByCategory(this.categoryName)
    else observable = this.challengeService.getChallenges()
    observable.subscribe(
      (challenges: Challenge[]) => {
        this.challenges = challenges
        this.getResolved();
      }
    )
  }

  get isAdmin() {
    return this.authService.isAdmin;
  }

  getResolved() {
    this.resolvedChallenges.map(resolved => {
      this.challenges.find(c => c.id == resolved.id).isResolved = true
    })
  }
  deleteChallenge(id){
    this.swService.displayConfirmation('Êtes-vous sûr de vouloir supprimer ce challenge ?', 'Cette action est irreversible', 
    'Le challenge a bien été supprimé', () => {
      this.challengeService.deleteChallenge(id).subscribe(() => {
        this.getChallenges()
      })
    })
  }
}
