import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { ChallengeService } from 'src/app/services/challenge.service';
import { ChallengeCategory } from 'src/app/models/challenge.model';
import { ProposalService } from 'src/app/services/proposal.service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {

  challengeCategories: ChallengeCategory[]

  constructor(private authService: AuthService,
              private challengeService: ChallengeService,
              private proposalService: ProposalService) { }

  ngOnInit() {
    this.getCategories()
    this.proposalService.getProposals()

  }

  get isAdmin() {
    return this.authService.isAdmin;
  }

  getCategories() {
    this.challengeService.getCategories().subscribe(
      (categories: ChallengeCategory[]) => this.challengeCategories = categories
    )
  }
  
  get proposalsCount() {
    return this.proposalService.proposals.filter(prop => prop.status == 'NEW').length
  }

}
