import { Component, OnInit, TemplateRef } from '@angular/core';
import { DocService } from 'src/app/services/doc.service';
import { Documentation } from 'src/app/models/documentation.model';
import { Subscription } from 'rxjs';
import { SweetAlertService } from 'src/app/services/sweet-alert.service';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-documentation-list',
  templateUrl: './documentation-list.component.html',
  styleUrls: ['./documentation-list.component.scss']
})
export class DocumentationListComponent implements OnInit {

  documentations: Documentation[]
  documentationsSubcription: Subscription
  bsModalRef: BsModalRef
  currentDoc: Documentation

  constructor(private docService: DocService,
              private swService: SweetAlertService,
              private modalService: BsModalService) { }

  ngOnInit() {
    this.documentationsSubcription = this.docService.docs$.subscribe((documentations: Documentation[]) => {
      this.documentations = documentations
    })
    this.docService.getDocs()
  }

  onDelete(fileName: String) {
    this.swService.displayConfirmation('Êtes-vous sûr de vouloir supprimer ce document ?',
      'Cette action est irréversible', 'Le document a bien été supprimé', () => {
        this.docService.deleteDoc(fileName)
        .subscribe(data => this.docService.getDocs())
      })
  }

  openPdf(template: TemplateRef<any>, doc: Documentation) {
    this.currentDoc = doc
    this.bsModalRef = this.modalService.show(template, { class: 'modal-lg' })
  }

  get iframeHeight() {
    return (window.innerHeight * 0.9).toString() + 'px'
  }

  getDocType(doc: Documentation) {
    return this.docService.getDocType(doc) == 'file' ? 'Document' : 'Page web'
  }
}
