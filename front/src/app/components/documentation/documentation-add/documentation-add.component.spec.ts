import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DocumentationAddComponent } from './documentation-add.component';

describe('DocumentationAddComponent', () => {
  let component: DocumentationAddComponent;
  let fixture: ComponentFixture<DocumentationAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DocumentationAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DocumentationAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
