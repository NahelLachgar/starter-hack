import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DocumentationAddFormComponent } from './documentation-add-form.component';

describe('DocumentationAddFormComponent', () => {
  let component: DocumentationAddFormComponent;
  let fixture: ComponentFixture<DocumentationAddFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DocumentationAddFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DocumentationAddFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
