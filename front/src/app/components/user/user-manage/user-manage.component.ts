import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from 'src/app/services/user.service';
import { User } from 'src/app/models/user.model';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service';
import { SweetAlertService } from 'src/app/services/sweet-alert.service';
import { ThrowStmt } from '@angular/compiler';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-user-manage',
  templateUrl: './user-manage.component.html',
  styleUrls: ['./user-manage.component.scss']
})
export class UserManageComponent implements OnInit {

  user: User;
  userForm: FormGroup;
  avatarForm: FormGroup;
  isEditing = false;
  fileToUpload: File;

  get password() { return this.userForm.controls['password'] }
  get passwordConfirm() { return this.userForm.controls['passwordConfirm'] }

  constructor(private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private userService: UserService,
    private router: Router,
    private authService: AuthService,
    private swService: SweetAlertService) { }

  ngOnInit() {
    const userId = this.route.snapshot.params.id
    if (userId) {
      this.isEditing = true
      this.userService.getUserById(userId).then((user: User) => {
        this.user = user
        this.initForms()
      })
    } else {
      this.initForms()
    }
  }

  initForms() {
    this.initUserForm()
    this.initAvatarForm()
  }

  initUserForm() {
    this.userForm = this.formBuilder.group({
      username: [{ value: this.user ? this.user.username : "", disabled: this.isEditing }, [Validators.required]],
      email: [this.user ? this.user.email : "", [Validators.required, Validators.email]],
      company: this.user ? this.user.company : "",
      password: ["", Validators.minLength(6)],
      passwordConfirm: "",
      school: this.user ? this.user.school : "",
      points: this.user ? this.user.points : 0,
      description: this.user ? this.user.description : "",
      role: this.user ? this.user.role : 'ADMIN'
    })
  }

  initAvatarForm() {
    this.avatarForm = this.formBuilder.group({
      avatar: null
    })
  }

  get isCurrentUser() {
    return this.user && this.user.id == this.authService.currentUserValue.id
  }

  get isPasswordConfirmValid() {
    return (!this.password.dirty || !this.passwordConfirm.dirty) ||
    (this.userForm.value.password == this.userForm.value.passwordConfirm) || this.userForm.value.password == ""
  }

  onSubmit() {
    let observable: Observable<User>
    if (this.userForm.invalid || !this.isPasswordConfirmValid) return false
    const user: User = this.userForm.value
    console.log(user)
    user.role = this.user ? this.user.role : 'ADMIN'
    if (this.isEditing) {
      user.username = this.user.username
      user.id = this.user.id
      observable = this.userService.updateUser(user)
    }
    else observable = this.userService.insertAdmin(user)

    observable.subscribe((user: User) => {
      this.userService.getUsers()
      const text = 'L\'utilisateur a été ' + (this.isEditing ? 'modifié !' : 'ajouté !')
      this.swService.sendAlert(text, 'success',
        () => {
          this.router.navigate(['/user-list']);
        })
    })
  }

  submitAvatar() {
    var formData = new FormData();
    formData.append("avatar", this.fileToUpload);
    this.userService.updateAvatar(formData).subscribe((user: User) => {
      const newUser = this.authService.currentUserValue
      newUser.photo = user.photo;
      this.authService.currentUserValue = newUser
    })
  }

  handleFileInput(files: FileList) {
    this.fileToUpload = files.item(0);
    this.readFile()
    this.submitAvatar()
  }

  readFile() {
    var reader = new FileReader();
    reader.readAsDataURL(this.fileToUpload);
    reader.onload = (_event) => {
      this.user.photo = reader.result.toString();
    }
  }
}
