import { Component, OnInit } from '@angular/core';
import { ProposalService } from 'src/app/services/proposal.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Proposal } from 'src/app/models/proposal.model';

@Component({
  selector: 'app-proposals-details',
  templateUrl: './proposals-details.component.html',
  styleUrls: ['./proposals-details.component.scss']
})
export class ProposalsDetailsComponent implements OnInit {
  proposal: Proposal
  proposals: Proposal[]
  id: number = this.route.snapshot.params.id

  constructor(private route: ActivatedRoute,
              private router: Router, 
              private proposalService: ProposalService) { }

  ngOnInit() {
    this.getProposal()
    this.proposalService.getProposals()
    this.proposalService.proposals$
      .subscribe((proposals) => this.proposals = proposals)
  }

  getProposal() {
    this.proposalService.getProposal(this.id)
      .subscribe(proposal => {
        if (proposal == null) this.router.navigateByUrl("/proposals")
        this.proposal = proposal
      },
      err => console.error(err))
  }

  onRemoveProposal() {
    this.proposalService.removeProposal(this.id)
      .subscribe(_ => 
        setTimeout(() => {
          this.router.navigateByUrl("/proposals")
        }, 1000),
      err => console.error(err))
  }

  onAcceptProposal() {
    this.proposalService.acceptProposal(this.id, 200, "ACCEPTED")
      .subscribe(_ => {
        this.router.navigateByUrl("/proposals")
      },
      err => console.error(err))
  }

  onProcessProposal() {
    this.router.navigate(["/challenges/add"], {
      queryParams: { proposal: this.id }
    })
  }

}
