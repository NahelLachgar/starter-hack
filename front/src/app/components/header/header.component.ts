import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { CloudflareService } from 'src/app/services/cloudflare.service';
import { User } from 'src/app/models/user.model';
import { Router } from '@angular/router';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  isUnderAttack: boolean
  user: User
  constructor(private authService: AuthService,private userService: UserService, private cloudflareService: CloudflareService, private router: Router) { 
  }

  ngOnInit() {
    if (this.isAdmin) {
      this.checkUnderAttack()
    }
    this.authService.currentUserSubject
    .subscribe(
      (user: User) => {
        this.user = user
      }
    );
  }

  get isAdmin() {
    return this.authService.isAdmin;
  }
  
  get isAuth() {
    return this.authService.currentUserValue;
  }
  toggleUnderAttack() {
    this.cloudflareService.toggleUnderAttack(this.isUnderAttack ? "medium" : "under_attack")
    this.isUnderAttack = !this.isUnderAttack
  }

  checkUnderAttack() {
    this.cloudflareService.getSecurityLevel(callback => {
      if (callback["result"]["value"] == "under_attack") 
        this.isUnderAttack = true
      else 
        this.isUnderAttack = false
    })
  }

  onSearch(search: String){
    console.log("test")
    if(!search) return false
    this.router.navigateByUrl('/RefreshComponent', { skipLocationChange: true }).then(() => {
      this.router.navigate(['/search'], {queryParams: {query: search}});
  }); 
  }

  settings(){
    this.router.navigateByUrl('/user/manage/'+this.user.id);
  }
  logout(){
    this.authService.logout()
  }
}
