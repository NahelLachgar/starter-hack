import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service';
import { first } from 'rxjs/operators';
import { User } from 'src/app/models/user.model';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  returnUrl: string
  form: FormGroup
  get email(){return this.form.get("email")}
  get password(){return this.form.get("password")}

  constructor(private route: ActivatedRoute,
     private formBuilder: FormBuilder,
     private authService: AuthService,
     private router: Router) { }

  ngOnInit() {
    this.returnUrl = this.route.snapshot.queryParams["returnUrl"] || "/"
    if(this.authService.currentUserValue) this.router.navigate([this.returnUrl])
    this.initForm()
  }

  initForm() {
    this.form = this.formBuilder.group({
      email: ["", Validators.required],
      password: ["", Validators.required]
    })
  }

  onSubmit(){
    if(this.form.invalid) return false
    this.authService.login(this.email.value, this.password.value)
    .pipe(first())
    .subscribe(
      (user: User) => {
        this.router.navigate([this.returnUrl])
        location.reload()
      },
      err => {
        console.error(err)
      }
    )
  }
}
