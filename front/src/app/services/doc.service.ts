import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Subject } from 'rxjs';
import { Documentation } from '../models/documentation.model';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DocService {

  s3Bucket = environment.awsS3Bucket
  s3Region = environment.awsS3Region
  apiUrl = environment.apiUrl;
  docs$ = new Subject<Documentation[]>()
  docs: Documentation[] = [];

  constructor(private http: HttpClient) { }

  emitDocs(){
    this.docs$.next(this.docs)
  }

  getDocType(doc: Documentation) {
    return doc.url.startsWith(`https://${this.s3Bucket}.s3.${this.s3Region}.amazonaws.com/`)
    ? "file" : "link"
  }

  getDocs(){
    return this.http.get<Documentation[]>(`${this.apiUrl}/documentations`).subscribe((docs: Documentation[]) => {
      this.docs = docs;
      this.emitDocs()
    })
  }

  insertDoc(documentation: Documentation, file?: File){
    let url = `${this.apiUrl}/documentations`
    if(file){
      url += "/doc"
      var formData = new FormData();
      formData.append('file', file, documentation.name);
      }
      return this.http.post<Documentation>(url, file ? formData : documentation)
  }

  getDocById(id: Number){
    return new Promise ((resolve, reject) => {
    this.http.get<Documentation>(`${this.apiUrl}/documentations/${id}`).subscribe((documentation : Documentation) => {
      resolve(documentation)},
      err => reject(err))
    })
  }

  deleteDoc(fileName: String){
    return this.http.delete(`${this.apiUrl}/documentations/${fileName}`)
  }

  editDoc(documentation : Documentation) {
    return this.http.put<Documentation>(`${this.apiUrl}/documentations/edit`, documentation)
  }

}
