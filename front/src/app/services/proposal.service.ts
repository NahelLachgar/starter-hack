import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Proposal } from '../models/proposal.model';
import { map } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { WebSocketService } from './websocket.service';

@Injectable({
  providedIn: 'root'
})
export class ProposalService {
  apiUrl = environment.apiUrl
  proposals$ = new Subject<Proposal[]>()
  proposals: Proposal[] = [];

  constructor(private http: HttpClient, private websocketService: WebSocketService) { 
    this.websocketService.subscribeToTopic("proposals", this.getProposals.bind(this))
  }

  emitProposals() {
    this.proposals$.next(this.proposals)
  }

  getProposals() {
    return this.http.get(`${this.apiUrl}/proposals`)
      .subscribe((proposals: Proposal[]) => {
        this.proposals = proposals
        this.emitProposals()
      }, error => {
        console.error(error)
      })
  }

  getProposal(id: number) {
    return this.http.get(`${this.apiUrl}/proposals/${id}`)
      .pipe(map((proposal: Proposal) => {
        return proposal
      }))
  }

  removeProposal(id: number) {
    return this.http.delete(`${this.apiUrl}/proposals/remove/${id}`)
  }

  acceptProposal(id: number, points: number, status: string) {
    return this.http.post(`${this.apiUrl}/proposals/accept/${id}`, { points, status })
  }
}
