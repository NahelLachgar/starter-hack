import { Injectable } from '@angular/core';
import Swal, { SweetAlertIcon } from 'sweetalert2';

@Injectable({
  providedIn: 'root'
})
export class SweetAlertService {

  constructor() { }

  sendAlert(text: string, icon: SweetAlertIcon, callback?: CallableFunction) {
    Swal.fire({ text: text, icon: icon })
      .then(_ => {
        if (callback) callback()
      });
  }

  displayConfirmation(title: string, text: string, doneText: string, callback: CallableFunction) {
    Swal.fire({
      title: title,
      text: text,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      cancelButtonText: 'Annuler',
      confirmButtonText: 'Confirmer'
    }).then((result) => {
      if (result.value) {
        Swal.fire(
          'Supprimé !',
          doneText,
          'success'
          )
          callback()
      }
    })
  }

  closeAlert() {
    return Swal.close()
  }
}