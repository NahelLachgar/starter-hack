import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, interval } from 'rxjs';
import { map, startWith, first } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { Router } from '@angular/router';
import { User } from '../models/user.model';
import { HttpClient } from '@angular/common/http';
import { WebSocketService } from './websocket.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  public currentUserSubject: BehaviorSubject<User>;
  public currentUser: Observable<User>;
  apiUrl = environment.apiUrl;



  constructor(private http: HttpClient, 
    private router: Router,
    private websocketService: WebSocketService) {

    this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentUser')));
    this.currentUser = this.currentUserSubject.asObservable();
    this.websocketService.subscribeToTopic("user", this.getUser.bind(this))
  }

  get isAdmin() {
    return (this.currentUserValue && this.currentUserValue.role == 'ADMIN' ||
      this.currentUserValue && this.currentUserValue.role == 'SUPERADMIN')
    }
    
    get isSuperAdmin() {
      return (this.currentUserValue && this.currentUserValue.role == 'SUPERADMIN')
  }

  public get currentUserValue(): User {
    return this.currentUserSubject.value;
  }

  public set currentUserValue(user: User) {

    if(this.currentUserValue && this.currentUserValue.token && user){
      user.token = this.currentUserValue.token
    }

    this.currentUserSubject.next(user);
    localStorage.setItem('currentUser', JSON.stringify(user));
  }

  getUser(){
    return new Promise ((resolve, reject) => {
      this.http.get<User>(`${this.apiUrl}/users/${this.currentUserValue.id}`).subscribe((user: User) => {
        this.currentUserValue = user
      }, err => reject(err))
    })
  }
 

  login(email: string, password: string) {
    return this.http.post<any>(`${this.apiUrl}/account/login`, { email, password })
      .pipe(map((user: User) => {
        // login successful if there's a jwt token in the response
        if (user && user.token) {
          // store user detail  s and jwt token in local storage to keep user logged in between page refreshes
          this.currentUserValue = user
        }
        return user;
      }));
  }

  register(username: string, email: string, password: string, passwordrep: string) {
    return this.http.post<any>(`${this.apiUrl}/account/register`, { username, email, password })

  }

  logout() {
    this.currentUserValue = null;
    this.router.navigate(['/login']);
  }
}
