import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
    providedIn: 'root'
  })
export class CloudflareService {
    apiUrl = environment.apiUrl;

    constructor(private http: HttpClient, private router: Router) { }

    getSecurityLevel(callback) {
        return this.http.get(`${this.apiUrl}/cloudflare/security-level`)
                .subscribe((securityLevel) => {
                    callback(securityLevel)
                })
    }

    toggleUnderAttack(securityLevel: string) {
        return this.http.patch(`${this.apiUrl}/cloudflare/security-level`, { security_level: securityLevel })
            .subscribe()
    }

    getDNSRecords() {
        return this.http.get(`${this.apiUrl}/cloudflare/dns`)
    }

    deleteUnusedDns() {
        return this.http.delete(`${this.apiUrl}/dns/unused`)
    }
}