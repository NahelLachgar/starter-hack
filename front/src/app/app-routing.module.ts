import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { AuthGuard } from './guards/auth.guard';
import { ChallengeListComponent } from './components/challenge/challenge-list/challenge-list.component';
import { ChallengeDetailsComponent } from './components/challenge/challenge-details/challenge-details.component';
import { ChallengeAddComponent } from './components/challenge/challenge-add/challenge-add.component';
import { UserListComponent } from './components/user/user-list/user-list.component';
import { UserManageComponent } from './components/user/user-manage/user-manage.component';
import { UserProfileComponent } from './components/user/user-profile/user-profile.component';
import { AdminGuard } from './guards/admin.guard';
import { ProposalsComponent } from './components/proposals/proposals.component';
import { ProposalsDetailsComponent } from './components/proposals/proposals-details/proposals-details.component';
import { ProposalsAddComponent } from './components/proposals/proposals-add/proposals-add.component';
import { DocumentationListComponent } from './components/documentation/documentation-list/documentation-list.component';
import { DocumentationAddComponent } from './components/documentation/documentation-add/documentation-add.component';
import { RankListComponent } from './components/rank/rank-list/rank-list.component';
import { SearchComponent } from './components/search/search.component';
import { CloudflareComponent } from './components/cloudflare/cloudflare.component';

const routes: Routes = [
  { path: "login", component: LoginComponent },
  { path: "register", component: RegisterComponent },
  { path: "user-list", component: UserListComponent, canActivate: [AdminGuard] },
  { path: "user/manage/:id", component: UserManageComponent,  canActivate: [AdminGuard] },
  { path: "user/add", component: UserManageComponent,  canActivate: [AuthGuard] },
  { path: "user/:id", component: UserProfileComponent},
  { path: "user", component: UserProfileComponent},
  { path: "challenges", component: ChallengeListComponent },
  { path: "challenges/add", component: ChallengeAddComponent, canActivate: [AdminGuard] },
  { path: "challenge/:id", component: ChallengeDetailsComponent },
  { path: "challenge/edit/:id", component: ChallengeAddComponent },
  { path: "proposals/add", component: ProposalsAddComponent },
  { path: "proposals", component: ProposalsComponent, canActivate: [AdminGuard] },
  { path: "proposals/:id", component: ProposalsDetailsComponent, canActivate: [AdminGuard] },
  { path: "documentations", component: DocumentationListComponent, canActivate: [AdminGuard] },
  { path: "documentation/add", component: DocumentationAddComponent, canActivate: [AdminGuard]},
  { path: "documentation/edit/:id", component: DocumentationAddComponent},
  { path: "search", component: SearchComponent },
  { path: "rank", component: RankListComponent, canActivate: [AuthGuard]},
  { path: "cloudflare", component: CloudflareComponent, canActivate: [AdminGuard]},
  { path: "", component: HomeComponent, pathMatch: "full", canActivate: [AuthGuard] },
  { path: "**", redirectTo: "" }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
