import { Pipe, PipeTransform } from '@angular/core';
import { Proposal } from '../models/proposal.model';

@Pipe({
    name: 'proposals',
})
export class ProposalsPipe implements PipeTransform {
    
    transform(proposals: Proposal[], status: string): any[] {
        if(proposals) {
            return proposals.filter(proposal => {
                return proposal.status === status;
            })
        }
    }

}