import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpErrorResponse } from '@angular/common/http';
import { AuthService } from '../services/auth.service';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { SweetAlertService } from '../services/sweet-alert.service';
import { environment } from 'src/environments/environment';

@Injectable()
export class JwtInterceptor implements HttpInterceptor {
    constructor(private authService: AuthService, private swService: SweetAlertService) { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        let currentUser = this.authService.currentUserValue;
        if (currentUser && currentUser.token)
            request = request.clone({ headers: request.headers.set("Authorization", 'Bearer ' + currentUser.token) });

        return next.handle(request).pipe(catchError(error => {
            if (error instanceof HttpErrorResponse && error.status === 403) {
                if (this.authService.currentUserValue) 
                    this.swService.sendAlert('Vous devez être connecté pour accéder à cette page', 'error')
                retry(1)
                this.authService.logout();
            } else {
                if (!environment.production) console.log(error)
                return throwError(error);
            }
        }));
    }
}