export class Feed {
    url: string
    title: string
    author: string
    description: string
    image: string
}