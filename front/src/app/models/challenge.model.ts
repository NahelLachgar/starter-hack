import { Documentation } from './documentation.model'
import { User } from './user.model'

export class ChallengesResponse {
    challenges: Challenge[]
}

export class ChallengeResponse {
    challenge: Challenge
}

export class Challenge {
    id?: number
    name: string
    description: string
    points: number
    level: number
    flag: String
    company: String
    type: string
    category: ChallengeCategory
    host: string
    timer: number
    docs: Documentation[]
    validationsCount?: number
    amiId?: string
    script?: string
    isResolved?: boolean
    contributor?: User
}

export class ChallengeCategory { 
    id?: number
    name: string
    icon?: string
}
