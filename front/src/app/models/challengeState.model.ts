import { Challenge } from './challenge.model'
import { User } from './user.model'

export class ChallengeState {
    id: number
    state: State
    challenge: Challenge
    osUsername: string
    osPassword?: string
    host: String
    user: User
    startDate: Date
    endDate: Date
    rating: number
    comment: string
  timer: number
}

export enum State {
    STARTED = "STARTED",
    RESOLVED="RESOLVED",
    FAILED="FAILED"
}