import { Challenge } from './challenge.model'
import { User } from './user.model'

export class Comment {
    id: number
    challenge: Challenge
    user: User
    rating: number
    comment: string
}
