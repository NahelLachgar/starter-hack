import { User } from './user.model'

export class Proposal {
    id: number
    title: string
    category: string
    description: string
    level: number
    status: string
    user: User
}