package fr.starterhack.back

import fr.starterhack.back.model.Person
import fr.starterhack.back.model.UserRole
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.springframework.boot.test.context.SpringBootTest

@SpringBootTest
class BackApplicationTests {

	@Test
	fun contextLoads() {
	}

	@Test()
	@DisplayName("Should create an user")
	fun createPerson() {
		val user = Person(
				username = "Username",
				password = "password",
				role = UserRole.USER,
				email = "email@gmail.com"
		);
		val list = listOf<Person>(user);
		Assertions.assertTrue(list.size == 1);
	}
}