package fr.starterhack.back

import fr.starterhack.back.service.user.IUserService
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Order
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest

@SpringBootTest
class DbTests (@Autowired var userService: IUserService){

    @Test
    @Order(1)
    @DisplayName("Should insert user in db")
    fun insertUserDb(){
        userService.insertUser("testt", "test@test.com", "password")
        Assertions.assertNotNull(userService.selectUserByEmail("test@test.com"))
    }

    @Test
    @Order(2)
    @DisplayName("Should delete an user in db from his username")
    fun deleteUserInDb(){
        userService.deleteUserByEmail("test@test.com")
        Assertions.assertNull(userService.selectUserByEmail("test@test.com"))
    }

    @Test
    @Order(3)
    @DisplayName("Should select user in db")
    fun selectUserDb() {
        userService.insertUser("testt", "test@test.com", "password")
        println("---------UserSelect${userService.selectUserByUsername("testt")}")
        Assertions.assertNotNull(userService.selectUserByUsername("testt"))
        userService.deleteUserByEmail("test@test.com")
    }


}