package fr.starterhack.back

import com.amazonaws.services.cloudformation.model.CreateStackRequest
import com.amazonaws.services.cloudformation.model.DescribeStacksResult
import fr.starterhack.back.service.AwsService
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import com.amazonaws.services.ec2.model.DescribeInstancesResult

@SpringBootTest
class AwsTests(@Autowired private val awsService: AwsService) {

    @Test()
    @DisplayName("Should create a stack")
    fun createCloudFormationStack() {
        val request: CreateStackRequest = CreateStackRequest()
                .withStackName("Test-Backend")
                .withTemplateURL("https://starterhack-storage.s3.eu-west-3.amazonaws.com/scripts/vpc_ec2-paris.yaml")
        awsService.createCloudFormationStack(request)
        val stack = awsService.getCloudFormationStack("Test-Backend")
        Assertions.assertTrue(stack!!.stacks[0].stackStatus=="CREATE_IN_PROGRESS")
    }

    @Test()
    @DisplayName("Should delete a stack")
    fun deleteCloudFormationStack() {
        awsService.deleteCloudFormationStack("Test-Backend")
        val stack = awsService.getCloudFormationStack("Test-Backend")
        Assertions.assertTrue(stack!!.stacks[0].stackStatus=="DELETE_IN_PROGRESS")
    }
}