package fr.starterhack.back

import fr.starterhack.back.service.CloudflareService
import org.junit.jupiter.api.Assertions
import org.springframework.util.Assert
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.test.context.SpringBootTest

const val TEST_DNS_RECORD = "test.starterhack.fr"

@SpringBootTest
class CloudflareTests(@Autowired private val cloudflareService: CloudflareService) {


    @Test
    @DisplayName("Should return dns records")
    fun getDnsRecords() {
        val dnsRecords = cloudflareService.getDnsRecords()
        Assert.hasText(dnsRecords, "\"success\": true")
    }

    @Test
    @DisplayName("Should return dns record")
    fun getDnsRecord() {
        cloudflareService.createDnsRecord("192.168.1.1", fullSubName = "test", isProxied = false)
        val getResponse = cloudflareService.getDnsRecord(TEST_DNS_RECORD)
        Assert.hasText(getResponse, "\"success\": true")
        cloudflareService.deleteDnsRecord(TEST_DNS_RECORD)
    }

    @Test
    @DisplayName("Should create a dns record")
    fun createDnsRecord() {
        val createResponse = cloudflareService.createDnsRecord("192.168.1.1", fullSubName = "test", isProxied = false)
        Assert.hasText(createResponse, "\"success\": true")
    }

    @Test
    @DisplayName("Should delete a dns record")
    fun deleteDnsRecord() {
        cloudflareService.createDnsRecord("192.168.1.1", fullSubName = "test", isProxied = false)
        val delResponse = cloudflareService.deleteDnsRecord(TEST_DNS_RECORD)
        Assertions.assertTrue(delResponse)
    }

    @Test
    @DisplayName("Should update security level")
    fun updateSecurityLevel() {
        val updateResponse = cloudflareService.updateSecurityLevel("medium")
        Assert.hasText(updateResponse, "\"success\": true")
    }

}