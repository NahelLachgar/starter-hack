package fr.starterhack.back.controller.user

import com.fasterxml.jackson.databind.ObjectMapper
import fr.starterhack.back.TestsUtils
import fr.starterhack.back.dao.UserRepository
import fr.starterhack.back.model.AuthViewModel
import fr.starterhack.back.model.Person
import fr.starterhack.back.security.CustomUserDetails
import fr.starterhack.back.service.jwt.JwtTokenService
import fr.starterhack.back.service.jwt.JwtUserDetailsService
import fr.starterhack.back.service.user.UserService
import org.junit.jupiter.api.*
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.springframework.test.annotation.DirtiesContext
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.MvcResult
import org.springframework.test.web.servlet.ResultActions
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultHandlers
import org.springframework.test.web.servlet.result.MockMvcResultMatchers
import org.springframework.web.multipart.MultipartFile

@ExtendWith(SpringExtension::class)
@SpringBootTest
@AutoConfigureMockMvc
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
class UserDeleteTest(
        @Autowired private val mvc: MockMvc,
        @Autowired private val userRepo: UserRepository,
        @Autowired private val testsUtils: TestsUtils) {

    val mapper = ObjectMapper()

    @BeforeEach
    fun insertPerson() {
        println(userRepo.findAll())
        testsUtils.insertUser()
    }

    fun deleteUser(jwt: String? = null): ResultActions {
        var token: String? = jwt ?: testsUtils.createToken(testsUtils.username)
        return mvc.perform(MockMvcRequestBuilders.delete("/users/${testsUtils.id}")
                .header("Authorization", "Bearer $token"))
    }

    fun deleteUserWithoutAuth(): ResultActions {
        return mvc.perform(MockMvcRequestBuilders.delete("/users/${testsUtils.id}"))
    }

    @Test()
    @DisplayName("Delete user")
    fun delete_user() {
        this.deleteUser()
                .andExpect(MockMvcResultMatchers.status().isOk)
        Assertions.assertNull(userRepo.findByUsername(testsUtils.username))
    }

    @Test()
    @DisplayName("Try to delete user with an other user account")
    fun delete_user_with_another_user_account_should_return_403() {
        val user = testsUtils.insertUser(Person(username = "TestUser1", password="testtest", email = "testmail@starterhack.fr"))
        val token = testsUtils.createToken(user.username)
        this.deleteUser(token)
                .andExpect(MockMvcResultMatchers.status().isForbidden)
        Assertions.assertNotNull(userRepo.findByUsername(testsUtils.username))
    }

    @Test()
    @DisplayName("Delete user with an other admin account")
    fun delete_user_with_another_admin_account() {
        val user = testsUtils.insertAdmin(Person(username = "TestUser1", password="testtest", email = "testmail@starterhack.fr"))
        val token = testsUtils.createToken(user.username)
        this.deleteUser(token)
                .andExpect(MockMvcResultMatchers.status().isOk)
        Assertions.assertNull(userRepo.findByUsername(testsUtils.username))
    }

    @Test()
    @DisplayName("Delete user with an other super admin account")
    fun delete_user_with_another_super_admin_account() {
        val user = testsUtils.insertSuperAdmin(Person(username = "TestUser1", password="testtest", email = "testmail@starterhack.fr"))
        val token = testsUtils.createToken(user.username)
        this.deleteUser(token)
                .andExpect(MockMvcResultMatchers.status().isOk)
        Assertions.assertNull(userRepo.findByUsername(testsUtils.username))
    }

    @Test()
    @DisplayName("Try to delete user without authentication")
    fun delete_user_without_authentication_should_return_403() {
        this.deleteUserWithoutAuth()
                .andExpect(MockMvcResultMatchers.status().isForbidden)
        Assertions.assertNotNull(userRepo.findByUsername(testsUtils.username))
    }
}