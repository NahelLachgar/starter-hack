package fr.starterhack.back.controller.account

import com.fasterxml.jackson.databind.ObjectMapper
import fr.starterhack.back.TestsUtils
import fr.starterhack.back.dao.UserRepository
import fr.starterhack.back.model.AuthViewModel
import fr.starterhack.back.model.Person
import fr.starterhack.back.security.CustomUserDetails
import fr.starterhack.back.service.jwt.JwtTokenService
import fr.starterhack.back.service.jwt.JwtUserDetailsService
import org.junit.jupiter.api.*
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.springframework.test.annotation.DirtiesContext
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.MvcResult
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultHandlers
import org.springframework.test.web.servlet.result.MockMvcResultMatchers


@ExtendWith(SpringExtension::class)
@SpringBootTest
@AutoConfigureMockMvc
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
class AccountPostRegisterTest(
        @Autowired private val mvc: MockMvc,
        @Autowired private val userRepo: UserRepository,
        @Autowired private val jwtTokenService: JwtTokenService,
        @Autowired private val jwtUserService: JwtUserDetailsService,
        @Autowired private val testsUtils: TestsUtils

) {

    val mapper = ObjectMapper()

    @Test()
    @DisplayName("Register")
    fun register_should_create_user() {

        val user = Person(
                username = testsUtils.username,
                email = testsUtils.email,
                password = testsUtils.password)
        val body = mapper.writeValueAsString(user)
        mvc.perform(MockMvcRequestBuilders.post("/account/register")
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(body))
                .andExpect(MockMvcResultMatchers.status().isOk)
        Assertions.assertNotNull(userRepo.findByUsername(testsUtils.username))
    }

    @Test()
    @DisplayName("Try to register with an already used username")
    fun register_with_used_username_should_return_400() {
        testsUtils.insertUser()
        val user = Person(
                username = testsUtils.username,
                email = "test2@starterhack.fr",
                password = testsUtils.password)
        val body = mapper.writeValueAsString(user)
        mvc.perform(MockMvcRequestBuilders.post("/account/register")
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(body))
                .andExpect(MockMvcResultMatchers.status().isBadRequest)
    }

    @Test()
    @DisplayName("Try to register with an already used email")
    fun register_with_used_email_should_return_400() {
        testsUtils.insertUser()
        val user = Person(
                username = "TestUser2",
                email = testsUtils.email,
                password = testsUtils.password)
        val body = mapper.writeValueAsString(user)
        mvc.perform(MockMvcRequestBuilders.post("/account/register")
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(body))
                .andExpect(MockMvcResultMatchers.status().isBadRequest)
    }

    @Test()
    @DisplayName("Try to register without username")
    fun register_without_username_should_return_400() {
        val user = Person(
                email = testsUtils.email,
                password = testsUtils.password)
        val body = mapper.writeValueAsString(user)
        mvc.perform(MockMvcRequestBuilders.post("/account/register")
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(body))
                .andExpect(MockMvcResultMatchers.status().isBadRequest)
    }

    @Test()
    @DisplayName("Try to register without password")
    fun register_without_password_should_return_400() {
        val user = Person(
                username = testsUtils.username,
                email = testsUtils.email)
        val body = mapper.writeValueAsString(user)
        mvc.perform(MockMvcRequestBuilders.post("/account/register")
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(body))
                .andExpect(MockMvcResultMatchers.status().isUnauthorized)
    }

    @Test()
    @DisplayName("Try to register without email")
    fun register_without_email_should_return_400() {
        val user = Person(
                username = testsUtils.username,
                password = testsUtils.password)
        val body = mapper.writeValueAsString(user)
        mvc.perform(MockMvcRequestBuilders.post("/account/register")
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(body))
                .andExpect(MockMvcResultMatchers.status().isBadRequest)
    }
}