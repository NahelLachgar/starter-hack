package fr.starterhack.back

import fr.starterhack.back.dao.UserRepository
import fr.starterhack.back.model.Person
import fr.starterhack.back.model.UserRole
import fr.starterhack.back.security.CustomUserDetails
import fr.starterhack.back.service.jwt.JwtTokenService
import fr.starterhack.back.service.jwt.JwtUserDetailsService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Primary
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.stereotype.Service

@Service
@Primary
class TestsUtils(
        @Autowired private val userRepo: UserRepository,
        @Autowired private val passwordEncoder: PasswordEncoder,
        @Autowired private val jwtUserService: JwtUserDetailsService,
        @Autowired private val jwtTokenService: JwtTokenService
        ) {

    var username = "TestUser"
    var password = "testPassword"
    var email = "testUser@starterhack.fr"
    var id = 2

    var adminUsername = "TestAdmin"
    var adminPassword = "testAdminPassword"
    var adminEmail = "testAdmin@starterhack.fr"

    var superAdminUsername = "TestSuperAdmin"
    var superAdminPassword = "testSuperAdminPassword"
    var superAdminEmail = "testSuperAdmin@starterhack.fr"



    fun insertAll() {
        insertUser()
        insertAdmin()
        insertSuperAdmin()
    }

    fun insertUser(user: Person? = null): Person {
        val userToSave = user ?: Person(username = username, password = password, email = email)
        userToSave.password = passwordEncoder.encode(userToSave.password)
        userToSave.role = UserRole.USER
        return userRepo.save(userToSave)
    }

    fun insertAdmin(user: Person? = null): Person {
        val userToSave = user ?: Person(username = adminUsername, password = adminPassword, email = adminEmail)
        userToSave.password = passwordEncoder.encode(userToSave.password)
        userToSave.role = UserRole.ADMIN
        return userRepo.save(userToSave)
    }

    fun insertSuperAdmin(user: Person? = null): Person {
        val userToSave = user ?: Person(username = superAdminUsername, password = superAdminPassword, email = superAdminEmail)
        userToSave.password = passwordEncoder.encode(userToSave.password)
        userToSave.role = UserRole.SUPERADMIN
        return userRepo.save(userToSave)
    }

    fun createToken(usernameOrEmail: String): String {
        var userDetails: CustomUserDetails? = jwtUserService.loadUserByUsername(usernameOrEmail) as CustomUserDetails?
                ?:  jwtUserService.loadUserByEmail(usernameOrEmail) as CustomUserDetails?
        return jwtTokenService.generateToken(userDetails!!)
    }
}