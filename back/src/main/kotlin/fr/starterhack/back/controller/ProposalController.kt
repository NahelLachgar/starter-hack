package fr.starterhack.back.controller

import com.fasterxml.jackson.databind.node.ObjectNode
import fr.starterhack.back.dao.ProposalRepository
import fr.starterhack.back.model.Proposal
import fr.starterhack.back.model.ProposalStatus
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.web.bind.annotation.*
import java.util.*
import javax.validation.Valid

@RestController
@RequestMapping("/proposals")
class ProposalController(
        @Autowired private val proposalRepository: ProposalRepository,
        @Autowired private val template: SimpMessagingTemplate
) {

    @GetMapping
    fun getProposals(): MutableIterable<Proposal> {
        return proposalRepository.findAll()
    }

    @GetMapping("{proposalId}")
    fun getProposal(@PathVariable proposalId: Int): Optional<Proposal> {
        return proposalRepository.findById(proposalId)
    }

    @PostMapping
    fun addProposal(@Valid @RequestBody proposal: Proposal): Proposal {
        template.convertAndSend("/topic/proposals", "new proposal received")
        return proposalRepository.save(proposal)
    }

    @PatchMapping("{proposalId}")
    fun updateProposal(@PathVariable proposalId: Int,
                       @Valid @RequestBody proposal: Proposal) : Proposal {
        val updatedProposal = proposalRepository
                .findById(proposalId)
                .get()
        updatedProposal.title = proposal.title
        updatedProposal.category = proposal.category
        updatedProposal.description = proposal.description
        updatedProposal.status = proposal.status
        updatedProposal.level = proposal.level

        return proposalRepository.save(updatedProposal)
    }

    @DeleteMapping("remove/{proposalId}")
    fun removeProposal(@PathVariable proposalId: Int) {
        return proposalRepository.deleteById(proposalId)
    }

    @PostMapping("accept/{proposalId}")
    fun acceptProposal(@PathVariable proposalId: Int,
                       @RequestBody data: ObjectNode): Proposal {
        val proposal = proposalRepository.findById(proposalId).get()
        val status =
                if (data.get("status").asText() == "ACCEPTED")
                    ProposalStatus.ACCEPTED
                else
                    ProposalStatus.PROCESSED
        proposal.status = status

        if (proposal.status == ProposalStatus.PROCESSED)
            proposal.user!!.addPoints(data.get("points").asInt())

        template.convertAndSend("/topic/proposals", "proposal ${proposal.id} updated")
        return proposalRepository.save(proposal)
    }

}