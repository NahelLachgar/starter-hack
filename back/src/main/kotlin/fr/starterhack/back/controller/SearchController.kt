package fr.starterhack.back.controller

import fr.starterhack.back.model.Challenge
import fr.starterhack.back.model.Person
import fr.starterhack.back.service.ChallengeService
import fr.starterhack.back.service.user.UserService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/search")
class SearchController(@Autowired var userService: UserService,
                       @Autowired var challengeService: ChallengeService) {

    @GetMapping("/user/{search}")
    fun searchUser(@PathVariable search: String): MutableIterable<Person?>{
        return userService!!.searchUser(search)
    }

    @GetMapping("/challenge/{search}")
    fun searchChallenge(@PathVariable search: String): MutableIterable<Challenge>?{
        return challengeService!!.searchChallenge(search)
    }
}