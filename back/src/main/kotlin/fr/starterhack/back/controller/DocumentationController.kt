package fr.starterhack.back.controller

import fr.starterhack.back.dao.DocumentationRepository
import fr.starterhack.back.model.Documentation
import fr.starterhack.back.service.AwsS3Service
import fr.starterhack.back.service.documentation.IDocumentationService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import org.springframework.web.multipart.MultipartFile

@RestController
@RequestMapping("/documentations")
class DocumentationController(  @Autowired var documentationService: IDocumentationService?,
                                @Autowired var awsS3Service: AwsS3Service,
                                @Autowired var documentationRepository: DocumentationRepository) {

    @GetMapping
    fun getDocumentation() : MutableIterable<Documentation> {
        return documentationService!!.getDocumentations()
    }

    @PostMapping()
    fun insertDocumentation(@RequestBody documentation: Documentation?): Documentation? {
        return documentationRepository.save(documentation!!)
    }

    @PostMapping("/doc")
    fun insertDocumentation(@RequestParam("file") file: MultipartFile): Documentation? {
        val url: String? = awsS3Service.uploadFile(file, "docs")
        var fileName = file.originalFilename;
        var doc = Documentation(name = fileName, url = url)
        return documentationService!!.insertDocumentation(doc, file)
    }

    @GetMapping("/{id}")
    fun getDocumentationById(@PathVariable id: Int): Documentation {
        return documentationService!!.getDocumentationById(id)
    }

    @DeleteMapping("{fileName}")
    fun deleteDocumentationById(@PathVariable fileName: String){
         documentationService!!.deleteDocumentationByFilename(fileName)
    }

    @PutMapping("/edit")
    fun updateDocumentation(@RequestBody documentation: Documentation){
        return documentationService!!.updateDocumentation(documentation)
    }

}