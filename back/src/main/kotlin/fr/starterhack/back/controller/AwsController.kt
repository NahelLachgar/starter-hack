package fr.starterhack.back.controller

import com.amazonaws.services.cloudformation.model.CreateStackRequest
import com.amazonaws.services.cloudformation.model.CreateStackResult
import com.amazonaws.services.cloudformation.model.DeleteStackResult
import com.amazonaws.services.cloudformation.model.DescribeStacksResult
import com.amazonaws.services.ec2.AmazonEC2
import com.amazonaws.services.ec2.model.AmazonEC2Exception
import com.amazonaws.services.ec2.model.DescribeInstancesResult
import fr.starterhack.back.model.response.Response
import fr.starterhack.back.service.AwsService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("aws")
class AwsController(
        @Autowired private val awsService: AwsService
) {

    @GetMapping("ec2")
    fun getEc2Instances(): DescribeInstancesResult {
        return awsService.getEc2Instances()
    }

    @GetMapping("ec2/{instanceId}")
    fun getEc2Instance(@PathVariable(name="instanceId") instanceId: String): ResponseEntity<Any> {
        return try {
            ResponseEntity(awsService.getEc2Instance(instanceId), HttpStatus.OK);
        } catch(e: AmazonEC2Exception) {
            ResponseEntity(Response(false, "No EC2 instance has been found with this id"), HttpStatus.NOT_FOUND)
        }
    }

    @GetMapping("cloudformation")
    fun getCloudFormationStacks(): DescribeStacksResult {
        return awsService.getCloudFormationStacks()
    }

    @GetMapping("cloudformation/{stackName}")
    fun getCloudFormationStack(@PathVariable(name="stackName") stackName: String): DescribeStacksResult? {
        return awsService.getCloudFormationStack(stackName);
    }

    @PostMapping("cloudformation")
    fun createCloudFormationStack(): CreateStackResult {
        val request: CreateStackRequest = CreateStackRequest()
                .withStackName("Test")
                .withTemplateURL("https://staterhack-storage.s3.us-east-2.amazonaws.com/scripts/vpc_ec2.yaml")
        return awsService.createCloudFormationStack(request);
    }

    @DeleteMapping("cloudformation/{stackName}")
    fun createCloudFormationStack(@PathVariable() stackName: String): DeleteStackResult {
        return awsService.deleteCloudFormationStack(stackName);
    }
}