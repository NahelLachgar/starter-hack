package fr.starterhack.back.controller

import com.amazonaws.services.s3.model.S3ObjectSummary
import fr.starterhack.back.dao.ChallengeCategoryRepository
import fr.starterhack.back.dao.ChallengeRepository
import fr.starterhack.back.dao.ChallengeStateRepository
import fr.starterhack.back.model.*
import fr.starterhack.back.model.response.Response
import fr.starterhack.back.security.CustomUserDetails
import fr.starterhack.back.service.AwsS3Service
import fr.starterhack.back.service.ChallengeService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.security.core.Authentication
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.web.bind.annotation.*
import org.springframework.web.client.HttpClientErrorException
import org.springframework.web.multipart.MultipartFile
import java.net.URLDecoder
import java.util.*
import javax.validation.Valid

@RestController
@RequestMapping("challenges")
class ChallengeController(
        @Value("\${S3_STORAGE_BUCKET}") private val s3Bucket: String,
        @Value("\${AWS_REGION}") private val s3Region: String,
        @Autowired private val challengeRepository: ChallengeRepository,
        @Autowired private val challengeStateRepository: ChallengeStateRepository,
        @Autowired private val challengeCategoryRepository: ChallengeCategoryRepository,
        @Autowired private val challengeService: ChallengeService,
        @Autowired private val awsS3Service: AwsS3Service
) {

    fun getCurrentPerson(): Person {
        val authentication: Authentication = SecurityContextHolder.getContext().authentication
        val userDetails: CustomUserDetails = authentication.principal as CustomUserDetails
        userDetails.user.password = "null"
        return userDetails.user
    }

    @GetMapping
    fun getChallenges(): List<Challenge> {
        return challengeService.getChallenges().filter {
            challenge ->
            !challenge.mustBeDelete && challenge.challengeUpdate == null
                    && challenge.enabled
        }
    }

    @GetMapping("{challengeId}")
    fun getChallenge(@PathVariable challengeId: Int): Challenge {
        return challengeService.getChallenge(challengeId).get()
    }

    @PostMapping("run/{challengeId}")
    fun runChallenge(@PathVariable challengeId: Int): ChallengeState {
        val user: Person = getCurrentPerson()
        if (challengeService.isChallengeAvailable(challengeId, user.id as Int)) {

            val challenge: Challenge? = challengeService.getChallenge(challengeId).orElse(null)
            if (challenge != null) {
                val challengeState = challengeService.runChallenge(challenge, user)
                challengeStateRepository.save(challengeState)
                challengeState.challenge.flag = ""
                return challengeState
            }
            throw HttpClientErrorException(HttpStatus.NOT_FOUND, "ID not found")
        }
        throw HttpClientErrorException(HttpStatus.BAD_REQUEST, "This user is already resolving this challenge")
    }

    @PostMapping("resolve/{challengeStateId}")
    fun resolveChallenge(@PathVariable challengeStateId: Int,
                         @RequestParam("flag") flag: String): ChallengeState {
         URLDecoder.decode(flag, "UTF-8")
        val state: ChallengeState = challengeStateRepository.findById(challengeStateId).get()
        val challenge = challengeService.getChallenge(state.challenge.id!!).get()
        if (challenge.flag == flag) {
            return challengeService.endChallenge(challengeStateId, true)
        }
        return state
    }

    @PostMapping
    fun addChallenge(@Valid @RequestBody challenge: Challenge) : Challenge {
        return challengeService.addChallenge(challenge)
    }

    @GetMapping("categories")
    fun getCategories(): MutableIterable<ChallengeCategory> {
        return challengeCategoryRepository.findAll()
    }

    @GetMapping("validationsCount/{challengeId}")
    fun getValidationsCount(@PathVariable("challengeId") id: Int): Int {
        return this.challengeService.getValidationsCount(id)
    }

    @GetMapping("resolved")
    fun getResolvedChallenges(): MutableList<Challenge> {
        val user: Person = getCurrentPerson()
        return this.challengeService.getResolvedChallenges(user.id as Int)
    }

    @GetMapping("resolved/{challengeId}")
    fun hasResolvedChallenge(@PathVariable("challengeId") id: Int): Boolean {
        val user: Person = getCurrentPerson()
        return this.challengeService.hasResolvedChallenge(user.id as Int, id)
    }

    @GetMapping("started/{challengeId}")
    fun getStartedChallenges(@PathVariable("challengeId") id: Int): ChallengeState? {
        val user: Person = getCurrentPerson()
        return this.challengeService.getStartedChallenge(user.id as Int, id)
    }

    @GetMapping("scripts")
    fun getScripts(): MutableList<S3ObjectSummary>? {
        return awsS3Service.getFilesInBucket("scripts/")
    }

    @PostMapping("script")
    fun uploadScript(@RequestParam("script") script: MultipartFile): ResponseEntity<Response> {
        var file = awsS3Service.getFile("scripts", script.originalFilename!!)
        if (file != null)
            return ResponseEntity(Response(false, "This script already exists"), HttpStatus.BAD_REQUEST)
        return ResponseEntity(Response(true, awsS3Service.uploadFile(script, "scripts").toString()), HttpStatus.OK)
    }
    
    @PutMapping()
    fun updateChallenge(@Valid @RequestBody challenge: Challenge) : Challenge {
        return challengeService.updateChallenge(challenge)
    }
    
    @DeleteMapping("{challengeId}")
    fun deleteChallenge(@PathVariable("challengeId") challengeId: Int) {
        return challengeService.deleteChallenge(challengeId)
    }
    
    @GetMapping("/category/{categoryName}")
    fun getByCategory(@PathVariable("categoryName") categoryName: String): List<Challenge>? {
        return challengeService.getChallengesByCategory(categoryName)
    }

    @PutMapping("/comment")
    fun comment(@Valid @RequestBody comment: Comment): Comment{

        challengeService.commentChallenge(comment)
        return comment
    }

    @GetMapping("/comments/{challengeId}")
    fun getComments(@PathVariable("challengeId") challengeId: Int): MutableList<Comment>{
        return challengeService.getComments(challengeId)
    }
    @DeleteMapping("/comments/{challengeId}")
    fun deleteComment(@PathVariable("challengeId") challengeId: Int){
        val user: Person = getCurrentPerson()
        return challengeService.deleteComment(challengeId, user.id!!)
    }
}