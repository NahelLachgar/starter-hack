package fr.starterhack.back.controller

import com.fasterxml.jackson.databind.node.ObjectNode
import fr.starterhack.back.model.ChallengeState
import fr.starterhack.back.model.State
import fr.starterhack.back.service.CloudflareService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("cloudflare")
class CloudflareController(
        @Autowired private val cloudflareService: CloudflareService
) {

    @GetMapping("/dns", produces = [MediaType.APPLICATION_JSON_VALUE])
    fun getDnsRecords(): String {
        return cloudflareService.getDnsRecords()
    }

    @DeleteMapping("/dns/unused", produces = [MediaType.APPLICATION_JSON_VALUE])
    fun getUnusedDnsRecords() {
        return cloudflareService.deleteUnusedDns()
    }

    @GetMapping("/dns/{name}", produces = [MediaType.APPLICATION_JSON_VALUE])
    fun getDnsRecord(@PathVariable name: String): String {
        return cloudflareService.getDnsRecord(name)
    }

    @GetMapping("/security-level", produces = [MediaType.APPLICATION_JSON_VALUE])
    fun getSecurityLevel(): String {
        return cloudflareService.getSecurityLevel()
    }

    @DeleteMapping("/dns/{name}")
    fun deleteDnsRecord(@PathVariable name: String): Boolean {
        return cloudflareService.deleteDnsRecord(name)
    }

    @PostMapping("/dns", produces = [MediaType.APPLICATION_JSON_VALUE])
    fun createDnsRecord(@RequestBody dnsParams: ObjectNode): String {
        if (dnsParams.get("prefix") == null) {
            return cloudflareService.createDnsRecord(
                    dnsParams.get("ec2Host").asText(),
                    fullSubName = dnsParams.get("fullSubName").asText(),
                    isProxied = false)
        } else {
            return cloudflareService.createDnsRecord(
                    dnsParams.get("ec2Host").asText(),
                    isProxied = false)
        }
    }

    @PatchMapping("/security-level", produces = [MediaType.APPLICATION_JSON_VALUE])
    fun updateSecurityLevel(@RequestBody securityLevel: ObjectNode): String {
        return cloudflareService.updateSecurityLevel(securityLevel.get("security_level").asText())
    }



}