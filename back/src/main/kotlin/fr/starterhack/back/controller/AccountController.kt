package fr.starterhack.back.controller

import fr.starterhack.back.dao.UserRepository
import fr.starterhack.back.model.AuthViewModel
import fr.starterhack.back.model.Person
import fr.starterhack.back.model.response.Response
import fr.starterhack.back.security.CustomUserDetails
import fr.starterhack.back.service.AwsS3Service
import fr.starterhack.back.service.jwt.JwtTokenService
import fr.starterhack.back.service.jwt.JwtUserDetailsService
import fr.starterhack.back.service.user.IUserService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.web.bind.annotation.*
import org.springframework.web.multipart.MultipartFile
import java.security.Principal
import javax.validation.Valid


@RestController
@RequestMapping("account")
class AccountController {

    @Autowired
    private var jwtTokenService: JwtTokenService? = null

    @Autowired
    var jwtUserService: JwtUserDetailsService? = null

    @Autowired
    var userService: IUserService? = null

    @Autowired
    var awsS3Service: AwsS3Service? = null

    @Autowired
    private val passwordEncoder: PasswordEncoder? = null

    @GetMapping("/user")
    fun getUser(principal: Principal): Person? {
        return userService?.selectUserByUsername(principal.name)
    }

    @PostMapping("/login")
    fun login(@RequestBody user: AuthViewModel): ResponseEntity<Any> {
        var userDetails: CustomUserDetails? = jwtUserService!!.loadUserByEmail(user.email) as CustomUserDetails?
        if (userDetails == null) {
            userDetails = jwtUserService!!.loadUserByUsername(user.email) as CustomUserDetails?
        }

        return if (userDetails != null && passwordEncoder!!.matches(user.password, userDetails.password)) {
            userDetails.user.token = jwtTokenService?.generateToken(userDetails)
            ResponseEntity(userDetails.user, HttpStatus.OK)
        } else
            ResponseEntity(Response(false, "Email ou mot de passe incorrect"), HttpStatus.UNAUTHORIZED)
    }

    @PostMapping("/register")
    fun register(@Valid @RequestBody user: Person): ResponseEntity<Response> {
        if (user.email.isNullOrEmpty().or(user.username.isNullOrEmpty())) {
            return ResponseEntity(
                    Response(false, "Veuillez renseigner votre pseudo et votre adresse email"),
                    HttpStatus.BAD_REQUEST)
        }
        if(userService?.selectUserByEmail(user.email) != null)
            return ResponseEntity(Response(false, "Email utilisée"), HttpStatus.BAD_REQUEST)

        if(userService?.selectUserByUsername(user.username) != null)
            return ResponseEntity(Response(false, "Username utilisé"), HttpStatus.BAD_REQUEST)
        return if (userService?.insertUser(user.username, user.email, user.password) != null)
            ResponseEntity(Response(true), HttpStatus.OK)
        else
            ResponseEntity(Response(false), HttpStatus.BAD_REQUEST)
    }

    @PutMapping("/avatar")
    fun uploadAvatar(@RequestParam("avatar") avatar: MultipartFile, principal: Principal) : ResponseEntity<Any> {
        val url: String? = awsS3Service?.uploadFile(avatar, "img")
        return ResponseEntity(userService?.updateUserPhoto(principal.name, url), HttpStatus.OK)
    }
}

