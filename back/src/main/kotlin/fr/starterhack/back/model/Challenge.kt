package fr.starterhack.back.model
import javax.persistence.*

@Entity
data class Challenge(
        @Id @GeneratedValue(strategy = GenerationType.IDENTITY) var id: Int? = null,
        @Column(unique=true) var name: String = "",
        @Lob
        var description: String = "",
        var points: Int = 0,
        var level: Int = 0,
        var flag: String = "",
        var amiId: String? = null,
        var script: String? = null,
        @ManyToOne var category: ChallengeCategory = ChallengeCategory(),
        var host: String? = null,
        var timer: Int = 3600,
        var enabled: Boolean = true,
        @ManyToMany var docs: List<Documentation>? = null,
        @ManyToOne var contributor: Person? = null,
        @Transient var challengeUpdate: Challenge? = null,
        var mustBeDelete: Boolean = false
)

enum class ChallengeType { SOLO, CTF }
