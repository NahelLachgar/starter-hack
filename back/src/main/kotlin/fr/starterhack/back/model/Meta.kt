package fr.starterhack.back.model


import com.fasterxml.jackson.annotation.JsonProperty

data class Meta(
    @JsonProperty("auto_added")
    val autoAdded: Boolean,
    @JsonProperty("managed_by_apps")
    val managedByApps: Boolean,
    @JsonProperty("managed_by_argo_tunnel")
    val managedByArgoTunnel: Boolean,
    @JsonProperty("source")
    val source: String
)