package fr.starterhack.back.model.response


import com.fasterxml.jackson.annotation.JsonProperty
import fr.starterhack.back.model.Result

data class DNSRecordResponse(
        @JsonProperty("errors")
    val errors: List<Any>,
        @JsonProperty("messages")
    val messages: List<Any>,
        @JsonProperty("result")
    val result: Result? = null,
        @JsonProperty("success")
    val success: Boolean
)