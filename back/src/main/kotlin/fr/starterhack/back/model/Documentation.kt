package fr.starterhack.back.model

import org.springframework.web.multipart.MultipartFile
import javax.persistence.*

@Entity
data class Documentation(
        @Id @GeneratedValue(strategy = GenerationType.IDENTITY) var id: Int? = null,
        @Column(unique=true) var name: String? = "",
        val displayName: String? = "",
        var url: String? = ""
)


