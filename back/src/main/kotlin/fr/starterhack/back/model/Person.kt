package fr.starterhack.back.model

import com.fasterxml.jackson.annotation.JsonIdentityInfo
import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonProperty
import javax.persistence.*
import javax.validation.constraints.Email
import javax.validation.constraints.NotBlank
import javax.validation.constraints.NotEmpty
import javax.validation.constraints.Size

@Entity
data class Person(
        @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
        var id: Int? = null,
        @get: Size(max=30)
        @Column(unique=true) var username: String = "",
        @get: Size(min=6)
        @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
        @get: NotEmpty(message = "Mot de passe vide")
        var password: String = "",
        var role: UserRole = UserRole.USER,
        @get: Email(message = "Email invalide")
        @Column(unique=true)
        var email: String = "",
        var school: String? = null,
        var company: String? = null,
        var photo: String = "https://starterhack-storage.s3.eu-west-3.amazonaws.com/img/default.jpg",
        @Lob
        var description: String? = null,
        var points: Int = 0,
        var token: String? = null
) {
    fun getRank() {
        if (points<100) Rank.STARTER
    }

    fun addPoints(value: Int) {
        this.points += value;
    }
}

enum class UserRole { SUPERADMIN, ADMIN, USER }

enum class Rank { STARTER, STARTER2, MIDDLE, PRO, HACKER }