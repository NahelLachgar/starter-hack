package fr.starterhack.back.model

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

@JsonIgnoreProperties(ignoreUnknown = true)
data class Feed(
        var url: String? = "",
        var title: String? = "",
        var link: String? = "",
        var author: String? = "",
        var description: String? = "",
        var image: String? = ""
)