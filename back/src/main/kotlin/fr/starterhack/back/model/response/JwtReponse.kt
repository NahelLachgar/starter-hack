package fr.starterhack.back.model.response

data class JwtReponse(
    var access_token: String? = null
)