package fr.starterhack.back.configuration

import fr.starterhack.back.dao.ChallengeCategoryRepository
import fr.starterhack.back.dao.ChallengeRepository
import fr.starterhack.back.dao.UserRepository
import fr.starterhack.back.model.Challenge
import fr.starterhack.back.model.ChallengeCategory
import fr.starterhack.back.model.Person
import fr.starterhack.back.model.UserRole
import fr.starterhack.back.service.documentation.DocumentationService
import fr.starterhack.back.service.user.IUserService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Component

@Component
class RunTime(
        @Autowired var userService: IUserService,
        @Autowired var challengeCategoryRepository: ChallengeCategoryRepository,
        @Autowired var documentationService: DocumentationService,
        @Autowired var challengeRepo: ChallengeRepository,
        @Autowired var userRepo: UserRepository,
        @Value("\${spring.datasource.platform}") private val dbSource: String
        ) {
    init {
        println(dbSource)

    documentationService.getDocumentations()
    val admin = Person(id= 1, username = "SuperAdmin", password = "Starterhack2020*", email = "superadmin@starterhack.fr", role = UserRole.SUPERADMIN)
    val user = userRepo.findByEmail("superadmin@starterhack.fr")
    if (user == null) userService.insertSuperAdmin(admin)
    println(userRepo.findAll())
    documentationService.getDocumentations()

    val categories = listOf(
            ChallengeCategory(1, "Web Client", "laptop-code"),
            ChallengeCategory(2, "Web Serveur"),
            ChallengeCategory(3, "Système"),
            ChallengeCategory(4, "Réseau"),
            ChallengeCategory(5, "Forensic"),
            ChallengeCategory(6, "Stéganographie"),
            ChallengeCategory(7, "Réaliste")
    )
    challengeCategoryRepository.saveAll(categories)

    val challenges = listOf(
            Challenge(id = 1, name = "Challenge web 6", flag = "qBd74:ztU&3Uq",
                    category = categories[0], host = "https://web-ch6.starterhack.fr", level = 0, points = 10,
                    description = "<h2>Challenge web 6</h2><p>Affichez le mot de passe pour réussir ce challenge</p>"),
            Challenge(id = 2, name = "Challenge web 7", flag = "yX#!kX9t%HnN7c*",
                    category = categories[0], host = "https://web-ch7.starterhack.fr", level = 1, points = 20,
                    description = "<h2>Challenge web 7</h2><p>Trouvez le mot de passe pour réussir ce challenge</p>")
    )
    challenges.forEach {
        run {
            val challenge = challengeRepo.findByName(it.name)
            if (challenge == null) challengeRepo.save(it)
        }
    }
    challengeRepo.saveAll(challenges)
}
    }
