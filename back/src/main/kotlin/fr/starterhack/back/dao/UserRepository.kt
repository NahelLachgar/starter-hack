package fr.starterhack.back.dao

import fr.starterhack.back.model.Person
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository
import retrofit2.http.Query

@Repository
interface UserRepository : CrudRepository<Person, Int> {
    fun findByEmail(email: String?): Person?
    fun findByUsername(username: String?): Person?
    fun findUserById(id: Int?): Person?
    fun deleteByEmail( email: String)
    fun findUserByUsernameIgnoreCaseLike(username: String?): MutableIterable<Person?>
    fun findAllByOrderByPointsDesc(): List<Person>
}