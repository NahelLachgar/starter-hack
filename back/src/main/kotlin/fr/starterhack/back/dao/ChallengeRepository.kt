package fr.starterhack.back.dao

import fr.starterhack.back.model.Challenge
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

@Repository
interface ChallengeRepository : CrudRepository<Challenge, Int> {
    fun findByName(name: String): Challenge?
    fun findByCategoryName(categoryName: String): MutableList<Challenge>?
    fun findChallengeByNameIgnoreCaseLike(search: String): MutableList<Challenge>?
}