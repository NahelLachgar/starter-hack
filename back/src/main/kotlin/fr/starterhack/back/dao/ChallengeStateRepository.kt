package fr.starterhack.back.dao

import fr.starterhack.back.model.Challenge
import fr.starterhack.back.model.ChallengeState
import fr.starterhack.back.model.State
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

@Repository
interface ChallengeStateRepository : CrudRepository<ChallengeState, Int> {
    fun findByChallengeId(challengeId: Int): MutableList<ChallengeState>
    fun findByChallengeIdAndState(challengeId: Int, state: State): MutableList<ChallengeState>
    fun findByUserIdAndState(userId: Int, state: State): MutableList<ChallengeState>;
    fun findByUserIdAndChallengeIdAndState(userId: Int, challengeId: Int, state: State): MutableList<ChallengeState>
    fun findByStateAndHost(state: State, host: String): ChallengeState?
}