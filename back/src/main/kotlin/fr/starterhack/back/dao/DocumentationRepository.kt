package fr.starterhack.back.dao

import fr.starterhack.back.model.Documentation
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

@Repository
interface DocumentationRepository: CrudRepository<Documentation, Int> {
    fun deleteByName(fileName: String)
}