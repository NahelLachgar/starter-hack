package fr.starterhack.back.service.jwt

import fr.starterhack.back.dao.UserRepository
import fr.starterhack.back.model.Person
import fr.starterhack.back.security.CustomUserDetails
import org.apache.tomcat.jni.User.username
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.core.userdetails.User
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.stereotype.Service
import javax.transaction.Transactional


@Service
class JwtUserDetailsService : UserDetailsService {

    @Autowired
    var userRepo: UserRepository? = null

    @Transactional
    override fun loadUserByUsername(username: String?): UserDetails {

        val user: Person = userRepo?.findByUsername(username) ?: throw UsernameNotFoundException(username)

        return CustomUserDetails(user)
    }

    @Transactional
    fun loadUserByEmail(email: String?): UserDetails? {

        val user: Person = userRepo?.findByEmail(email) ?: return null

        return CustomUserDetails(user)
    }
}