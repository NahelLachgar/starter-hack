package fr.starterhack.back.service.user

import fr.starterhack.back.model.update.UpdateUser
import fr.starterhack.back.dao.UserRepository
import fr.starterhack.back.model.Person
import fr.starterhack.back.model.UserRole
import fr.starterhack.back.model.UserRole.ADMIN
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Primary
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.stereotype.Service
import java.net.URLEncoder.encode
import javax.transaction.Transactional


@Service
@Primary
class UserService(
        @Value("\${S3_STORAGE_BUCKET}") private val s3Bucket: String,
        @Value("\${AWS_REGION}") private val s3Region: String,
        @Autowired override var userRepo: UserRepository? = null,
        @Autowired private val passwordEncoder: PasswordEncoder? = null
) : IUserService{

    @Transactional
    override fun insertUser(username: String,email:String, password: String): Person? {
        val user = Person(
                username = username,
                email = email,
                password = passwordEncoder!!.encode(password)
        )
        return userRepo?.save(user)
    }

    @Transactional
    override fun selectUser(email: String, password: String): Person? {
        var user: Person? = userRepo?.findByEmail(email)
        return if(user != null){
            if(passwordEncoder!!.matches(password, user.password)){
                user;
            }
            else
                null
        }
        else
            null
    }

    @Transactional
    override fun selectUserByEmail(email: String): Person? {
        return userRepo?.findByEmail(email)
    }

    @Transactional
    override fun selectUserById(id: Int): Person?{
        return userRepo?.findUserById(id)
    }

    @Transactional
    override fun selectAllPerson(): MutableIterable<Person>? {
        return userRepo?.findAll()
    }

    @Transactional
    override fun selectUserByUsername(username: String): Person? {
        return userRepo?.findByUsername(username)
    }

    @Transactional
    override fun setUserRole(username: String, role: UserRole) {
        var user = userRepo?.findByUsername(username)
        user?.role = role
    }

    @Transactional
    override fun updateUser(username: String, user: UpdateUser): Person? {
        var userToUpdate = userRepo?.findByUsername(username)

        if(user.password != ""){
            if(user.password.equals(user.passwordConfirm)){
                userToUpdate?.password = passwordEncoder!!.encode(user.password)
            }
            else{
                throw Exception("Les mots de passes ne se correspondent pas")
            }
        }

        userToUpdate?.email = user.email
        userToUpdate?.company = user.company
        userToUpdate?.description = user.description
        userToUpdate?.school = user.school

        userRepo?.save(userToUpdate!!)

        return userToUpdate
    }

    @Transactional
    override fun updateUserPhoto(username: String, photoUrl: String?): Person? {
        val userToUpdate = userRepo?.findByUsername(username)

        if(photoUrl != null)
            userToUpdate?.photo = encode(photoUrl, java.nio.charset.StandardCharsets.UTF_8.toString())

        userRepo?.save(userToUpdate!!)

        return userToUpdate
    }
    
    @Transactional
    override fun deleteUserByEmail(email: String) {
        return userRepo!!.deleteByEmail(email)
    }

    @Transactional
    override fun updateUser(user: Person, isUser: Boolean) {
        var userUpdate = userRepo?.findUserById(user.id)

        userUpdate?.username = user.username
        userUpdate?.email = user.email
        userUpdate?.company = user.company
        if (!isUser) userUpdate?.points = user.points
        userUpdate?.description = user.description
        userUpdate?.school = user.school
        if (isUser) userUpdate?.password = passwordEncoder?.encode(user.password)!!

        userRepo?.save(userUpdate!!)
    }

    @Transactional
    override fun deleteUserAsAdmin(id: Int){
        userRepo?.deleteById(id)
    }

    @Transactional
    override fun insertSuperAdmin(user: Person): Person {
        user.password = passwordEncoder?.encode(user.password)!!
        return userRepo?.save(user) as Person
    }

    @Transactional
    override fun insertAdminAsAdmin(user: Person): Person{

        //Check if username already exist
        if(userRepo?.findByUsername(user.username) != null)
            println("Username already used")

        // Check if email already exist
        if(userRepo?.findByEmail(user.email) != null)
            println("Email already exist")
        if (user.password.isNullOrEmpty()) user.password = "Starterhack2020*"
        // Encode the password
        try {
            user.password = passwordEncoder!!.encode(user.password)
        }
        catch (e: Exception){
            println(e)
        }
        user.role = ADMIN

        userRepo?.save(user)

        return user
    }

    @Transactional
    override fun selectUsersOrderedByPoints(): List<Person> {
        return userRepo!!.findAllByOrderByPointsDesc()
    }

    override fun searchUser(search: String): MutableIterable<Person?> {
        return userRepo!!.findUserByUsernameIgnoreCaseLike("%$search%")
    }
}